{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "evils";
  home.homeDirectory = "/home/evils";

  home.packages = with pkgs; [
    audacity
    bc
    beep
    binutils
    bmap-tools
    file
    #firefox-wayland
    flashrom
    freecad
    gimp
    imagemagick
    inkscape
    iperf3
    jq
    kicad-unstable
    krita
    libreoffice
    liquidctl
    lshw
    lsof
    man-pages
    man-pages-posix
    matterhorn
    mattermost-desktop
    mosquitto
    mpv
    neofetch
    nix-update
    nixos-generators
    nixos-shell
    nixpkgs-fmt
    nixpkgs-review
    nmap
    openscad
    orpie
    picocom
    piper
    pinta
    playerctl
    pv
    ripgrep
    rsync
    # until #140607 is in nixos-unstable
    rxvt-unicode
    screen
    shellcheck
    signal-desktop
    speedtest-cli
    stress
    sway-contrib.grimshot
    tdesktop
    termdown
    thefuck
    tixati
    torbrowser
    unzip
    vlc
    wev
    yj
    youtube-dl
    zathura
    zip
    zstd
  ];

  programs.nix-index.enable = true;
  programs.direnv.enable = true;
  programs.fzf.enable = true;

  programs.git = {
    enable = true;
    userName = "Evils";
    userEmail = "evils.devils@hotmail.com";
    extraConfig = {
      "core" = {
        "editor" = "vim";
      };
      "push" = {
        "default" = "simple";
      };
      "init" = {
        "defaultBranch" = "main";
      };
      "url" = {
        "https://github.com/" = {
          "insteadOf" = [
            "gh:"
            "github:"
          ];
        };
        "https://gitlab.com/" = {
          "insteadOf" = [
            "gl:"
            "gitlab:"
          ];
        };
      };
    };
  };

/*
  # this results in automatic insertion of tabs/spaces, even in pastes
  # that doesn't appear to be correctible
  # it also adds a white background to the bar at the bottom ¯\_(ツ)_/¯
  # and unsetting mouse stuff isn't an option, somehow
  # and it's not clear if the undoDir setting would create the directory if it doesn't exist
  programs.vim = {
    enable = true;
    extraConfig = ''
      set mouse=
      syntax on

      " Put plugins and dictionaries in this dir (also on Windows)
      let vimDir = '$HOME/.vim'

      if stridx(&runtimepath, expand(vimDir)) == -1
          " vimDir is not on runtimepath, add it
          let &runtimepath.=','.vimDir
      endif

      " Keep undo history across sessions by storing it in a file
      if has('persistent_undo')
          let myUndoDir = expand(vimDir . '/undodir')
          " Create dirs
          call system('mkdir ' . vimDir)
          call system('mkdir ' . myUndoDir)
          let &undodir = myUndoDir
          set undofile
      endif
    '';
  };
*/

  programs.alacritty = {
    enable = true;
    settings = {
      background_opacity = 0.60;
      
      mouse.hide_when_typing = true;
      
      cursor = {
        style = {
          shape = "Underline";
          blinking = "On";
        };
        unfocused_hollow = false;
        blink_interval = 600;
      };
      
      font = {
        family = "Noto Sans Mono";
        size = 10;
      };
      
      scrolling = {
        history = 100000;
        multiplier = 3;
      };
      
      key_bindings = [
        { key = "Return"; mods = "Control|Shift"; action = "SpawnNewInstance"; }
        { key = "PageUp"; mods = "Shift"; action = "ScrollPageUp"; }
        { key = "PageDown"; mods = "Shift"; action = "ScrollPageDown"; }
        { key = "End"; mods = "Shift"; action = "ScrollToBottom"; }
        { key = "Home"; mods = "Shift"; action = "ScrollToTop"; }
      ];
      
      schemes = {
        evils = {
          # started as snazzy
          # Default colors
          primary = {
            background = "#282828";
            foreground = "#ffffff";
          };
          cursor.cursor = "#ffffff";
          # Normal colors
          normal = {
            black =	"#000000";
            red =	"#ff5c57";
            green =	"#a8e827";
            yellow =	"#e3d329";
            blue =	"#66ccff";
            magenta =	"#e65ce6";
            cyan =	"#3fd9d9";
            white =	"#ffffff";
          };
          # Bright colors
          # mostly the same as the normal ones
          bright = {
            # this black is used for zsh suggestions
            black =	"#727969";
            red =	"#ff5c57";
            green =	"#a8e827";
            # this yellow is used by vim for comment keywords
            # NOTE yellow
            yellow =	"#e3d329";
            blue =	"#66ccff";
            magenta =	"#e65ce6";
            cyan =	"#3fd9d9";
            white =	"#ffffff";
          };
        };
      };
      
      # normally this is a reference to the "evils" color scheme above...
      # colors = evils;
      colors = {
        # started as snazzy
        # Default colors
        primary = {
          background = "#282828";
          foreground = "#ffffff";
        };
        cursor.cursor = "#ffffff";
        # Normal colors
        normal = {
          black =	"#000000";
          red =		"#ff5c57";
          green =	"#a8e827";
          yellow =	"#e3d329";
          blue =	"#66ccff";
          magenta =	"#e65ce6";
          cyan =	"#3fd9d9";
          white =	"#ffffff";
        };
        # Bright colors
        # mostly the same as the normal ones
        bright = {
          # this black is used for zsh suggestions
          black =	"#727969";
          red =		"#ff5c57";
          green =	"#a8e827";
          # this yellow is used by vim for comment keywords
          # NOTE yellow
          yellow =	"#e3d329";
          blue =	"#66ccff";
          magenta =	"#e65ce6";
          cyan =	"#3fd9d9";
          white =	"#ffffff";
        };
      };
    };
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11";
}
